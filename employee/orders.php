<?php
session_start();
if (!(isset($_SESSION["myusername"]) && isset($_SESSION["employee_login"]))) {
    header("location:index.php");
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Delivery Page</title>
        <link rel="stylesheet" type="text/css" href="../frame.css" />
    </head>
    <center>
        <body>
            <div id="header"></div>
            <div id="buttons">
                <div class="button">
                    <a href="menu.php">Menu</a>
                </div>
                <div class="button">
                    <a href="orders.php">Deliveries</a>
                </div>
                <div class="button">
                    <a href='vehicles.php'>Change Vehicles</a>
                </div>
                <div class="button">
                    <a href='worktime.php'>Record Work Time</a>
                </div>
                <div class="button">
                    <a href='logout.php'>Logout</a>
                </div>
            </div>

            <?php
            ob_start();
            $host = "localhost"; // Host name 
            $username = "semsc"; // Mysql username 
            $password = "semsc"; // Mysql password 
            $db_name = "pizza"; // Database name 
            $tbl_name = "customer"; // Table name
// Connect to server and select databse.
            mysql_connect($host, $username, $password) or die("cannot connect");
            mysql_select_db($db_name) or die("cannot select DB");

            $sql = "Select orders.Order_ID, customer.Name_Last, customer.Name_First, orders.Payment_type, customer.Address, orders.Expected, orders.Price 
                from orders, employee, customer 
                where orders.driver ='" . $_SESSION['myusername'] . "' and orders.Driver=employee.Username and orders.Customer=customer.Username and orders.Paid_Time is null and Received is not null";

            $result = mysql_query($sql);

            $count = mysql_num_rows($result);
            $i = 0;

            if ($count == 0) {
                echo "You have made your deliveries!<br>";
            } else {
                echo "<form name='form1' method='post' action='update_orders.php'><table class='table1' width='820'>";
                echo "<tr><td colspan='7'><strong>Deliver These:</strong></td></tr>";
                echo "<tr><td>Order<br>ID</td><td>Customer<br>Name</td><td>Payment<br>Type</td><td>Address</td><td>Expected<br>Delivery Time</td><td>Paid Time</td><td>Price</td></tr>";
                while ($row = mysql_fetch_array($result)) {
                    $order_id = $row['Order_ID'];
                    echo "<tr>";
                    echo "<td>" . $row['Order_ID'] . "</td>";
                    echo "<td>" . $row['Name_Last'] . ", " . $row['Name_First'] . "</td>";
                    echo "<td>" . $row['Payment_type'] . "</td>";
                    echo "<td>" . $row['Address'] . "</td>";
                    echo"<td>" . $row['Expected'] . "</td>";
                    echo "<td>

<select name='year[$order_id]' id='year$i'>
<option value='blank'>  </option>
<option value='2012'>2012</option>
<option value='2013'>2013</option>
<option value='2014'>2014</option>
</select>
/
<select name='month[$order_id]' id='month$i'>
<option value='blank'>  </option>
<option value='01'>Jan</option>
<option value='02'>Feb</option>
<option value='03'>Mar</option>
<option value='04'>Apr</option>
<option value='05'>May</option>
<option value='06'>Jun</option>
<option value='07'>Jul</option>
<option value='08'>Aug</option>
<option value='09'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
/
<select name='day[$order_id]' id='day$i'>
<option value='blank'>  </option>
<option value='01'>1</option>
<option value='02'>2</option>
<option value='03'>3</option>
<option value='04'>4</option>
<option value='05'>5</option>
<option value='06'>6</option>
<option value='07'>7</option>
<option value='08'>8</option>
<option value='09'>9</option>
<option value='10'>10</option>
<option value='11'>11</option>
<option value='12'>12</option>
<option value='13'>13</option>
<option value='14'>14</option>
<option value='15'>15</option>
<option value='16'>16</option>
<option value='17'>17</option>
<option value='18'>18</option>
<option value='19'>19</option>
<option value='20'>20</option>
<option value='21'>21</option>
<option value='22'>22</option>
<option value='23'>23</option>
<option value='24'>24</option>
<option value='25'>25</option>
<option value='26'>26</option>
<option value='27'>27</option>
<option value='28'>28</option>
<option value='29'>29</option>
<option value='30'>30</option>
<option value='31'>31</option>
</select>
<br>
<select name='hour[$order_id]' id='hour$i'>
<option value='blank'>  </option>
<option value='0'>0</option>
<option value='1'>1</option>
<option value='2'>2</option>
<option value='3'>3</option>
<option value='4'>4</option>
<option value='5'>5</option>
<option value='6'>6</option>
<option value='7'>7</option>
<option value='8'>8</option>
<option value='9'>9</option>
<option value='10'>10</option>
<option value='11'>11</option>
<option value='12'>12</option>
<option value='13'>13</option>
<option value='14'>14</option>
<option value='15'>15</option>
<option value='16'>16</option>
<option value='17'>17</option>
<option value='18'>18</option>
<option value='19'>19</option>
<option value='20'>20</option>
<option value='21'>21</option>
<option value='22'>22</option>
<option value='23'>23</option>
</select>
:
<select name='minute[$order_id]' id='minute$i'>
<option value='blank'>  </option>
<option value='0'>0</option>
<option value='1'>1</option>
<option value='2'>2</option>
<option value='3'>3</option>
<option value='4'>4</option>
<option value='5'>5</option>
<option value='6'>6</option>
<option value='7'>7</option>
<option value='8'>8</option>
<option value='9'>9</option>
<option value='10'>10</option>
<option value='11'>11</option>
<option value='12'>12</option>
<option value='13'>13</option>
<option value='14'>14</option>
<option value='15'>15</option>
<option value='16'>16</option>
<option value='17'>17</option>
<option value='18'>18</option>
<option value='19'>19</option>
<option value='20'>20</option>
<option value='21'>21</option>
<option value='22'>22</option>
<option value='23'>23</option>
<option value='24'>24</option>
<option value='25'>25</option>
<option value='26'>26</option>
<option value='27'>27</option>
<option value='28'>28</option>
<option value='29'>29</option>
<option value='30'>30</option>
<option value='31'>31</option>
<option value='32'>32</option>
<option value='33'>33</option>
<option value='34'>34</option>
<option value='35'>35</option>
<option value='36'>36</option>
<option value='37'>37</option>
<option value='38'>38</option>
<option value='39'>39</option>
<option value='40'>40</option>
<option value='41'>41</option>
<option value='42'>42</option>
<option value='43'>43</option>
<option value='44'>44</option>
<option value='45'>45</option>
<option value='46'>46</option>
<option value='47'>47</option>
<option value='48'>48</option>
<option value='49'>49</option>
<option value='50'>50</option>
<option value='51'>51</option>
<option value='52'>52</option>
<option value='53'>53</option>
<option value='54'>54</option>
<option value='55'>55</option>
<option value='56'>56</option>
<option value='57'>57</option>
<option value='58'>58</option>
<option value='59'>59</option>
</select>


                    </td>";
                    echo"<td>" . $row['Price'] . "</td>";
                    echo "</tr>";
                    $i = $i + 1;
                }
                echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    <td><input type='submit' name='Update' value='Update'></td>";
                echo "</table></form>";
            }


            $sql = "Select orders.Order_ID, customer.Name_Last, customer.Name_First, orders.Payment_type, orders.Received, orders.Expected, orders.Price 
                from orders, customer 
                where orders.driver is null and orders.Customer=customer.Username";
            $result = mysql_query($sql);

            $count = mysql_num_rows($result);

            if ($count == 0) {
                echo "All orders have been delivered!";
            } else {
                echo "<form name='form2' method='post' action='get_orders.php'><table class='table1' width='820'>";
                echo "<tr><td colspan='7'><strong>New Orders:</strong></td></tr>";
                echo "<tr><td>Take</td><td>Customer Name</td><td>Payment Type</td><td>Order Placement Time</td><td>Expected Deliverly Time</td><td>Price</td></tr>";
                while ($row = mysql_fetch_array($result)) {
                    echo "<tr>";
                    echo "<td><input name='checks[]' value='" . $row['Order_ID'] . "' type='checkbox' /></td>";
                    echo "<td>" . $row['Name_Last'] . ", " . $row['Name_First'] . "</td>";
                    echo "<td>" . $row['Payment_type'] . "</td>";
                    echo "<td>" . $row['Received'] . "</td>";
                    echo "<td>" . $row['Expected'] . "</td>";
                    echo "<td>" . $row['Price'] . "</td>";
                    echo "</tr>";
                }
                echo "<td><input type='submit' name='Take' value='Take'></td>";
                echo "</table></form>";
            }
            
            ob_end_flush();
            ?>
        </body>
    </center>
</html>
