<?php
session_start();
if (!(isset($_SESSION["myusername"]) && isset($_SESSION["employee_login"]))) {
    header("location:index.php");
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Record Work Time</title>
        <link rel="stylesheet" type="text/css" href="../frame.css" />
    </head>
    <center>
        <body>
            <div id="header"></div>
            <div id="buttons">
                <div class="button">
                    <a href="menu.php">Menu</a>
                </div>
                <div class="button">
                    <a href="orders.php">Deliveries</a>
                </div>
                <div class="button">
                    <a href='vehicles.php'>Change Vehicles</a>
                </div>
                <div class="button">
                    <a href='worktime.php'>Record Work Time</a>
                </div>
                <div class="button">
                    <a href='logout.php'>Logout</a>
                </div>
            </div>
            <div id="login">
                <table width="300" border="0" align="center" cellpadding="0" cellspacing="1">
                    <tr>
                    <form name="form1" method="post" action="checkwork.php">
                        <td>
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td colspan="3"><strong>Insert Times</strong></td>
                                </tr>
                                <tr>
                                    <td width="78">Start</td>
                                    <td width="6">:</td>
                                    <td width="294">
                                        <select name='year1' id='year1'>
                                            <option value='blank'>  </option>
                                            <option value='2012'>2012</option>
                                            <option value='2013'>2013</option>
                                            <option value='2014'>2014</option>
                                        </select>
                                        /
                                        <select name='month1' id='month1'>
                                            <option value='blank'>  </option>
                                            <option value='01'>Jan</option>
                                            <option value='02'>Feb</option>
                                            <option value='03'>Mar</option>
                                            <option value='04'>Apr</option>
                                            <option value='05'>May</option>
                                            <option value='06'>Jun</option>
                                            <option value='07'>Jul</option>
                                            <option value='08'>Aug</option>
                                            <option value='09'>Sep</option>
                                            <option value='10'>Oct</option>
                                            <option value='11'>Nov</option>
                                            <option value='12'>Dec</option>
                                        </select>
                                        /
                                        <select name='day1' id='day1'>
                                            <option value='blank'>  </option>
                                            <option value='01'>1</option>
                                            <option value='02'>2</option>
                                            <option value='03'>3</option>
                                            <option value='04'>4</option>
                                            <option value='05'>5</option>
                                            <option value='06'>6</option>
                                            <option value='07'>7</option>
                                            <option value='08'>8</option>
                                            <option value='09'>9</option>
                                            <option value='10'>10</option>
                                            <option value='11'>11</option>
                                            <option value='12'>12</option>
                                            <option value='13'>13</option>
                                            <option value='14'>14</option>
                                            <option value='15'>15</option>
                                            <option value='16'>16</option>
                                            <option value='17'>17</option>
                                            <option value='18'>18</option>
                                            <option value='19'>19</option>
                                            <option value='20'>20</option>
                                            <option value='21'>21</option>
                                            <option value='22'>22</option>
                                            <option value='23'>23</option>
                                            <option value='24'>24</option>
                                            <option value='25'>25</option>
                                            <option value='26'>26</option>
                                            <option value='27'>27</option>
                                            <option value='28'>28</option>
                                            <option value='29'>29</option>
                                            <option value='30'>30</option>
                                            <option value='31'>31</option>
                                        </select>
                                        &nbsp;
                                        <select name='hour1' id='hour1'>
                                            <option value='blank'>  </option>
                                            <option value='0'>0</option>
                                            <option value='1'>1</option>
                                            <option value='2'>2</option>
                                            <option value='3'>3</option>
                                            <option value='4'>4</option>
                                            <option value='5'>5</option>
                                            <option value='6'>6</option>
                                            <option value='7'>7</option>
                                            <option value='8'>8</option>
                                            <option value='9'>9</option>
                                            <option value='10'>10</option>
                                            <option value='11'>11</option>
                                            <option value='12'>12</option>
                                            <option value='13'>13</option>
                                            <option value='14'>14</option>
                                            <option value='15'>15</option>
                                            <option value='16'>16</option>
                                            <option value='17'>17</option>
                                            <option value='18'>18</option>
                                            <option value='19'>19</option>
                                            <option value='20'>20</option>
                                            <option value='21'>21</option>
                                            <option value='22'>22</option>
                                            <option value='23'>23</option>
                                        </select>
                                        :
                                        <select name='minute1' id='minute1'>
                                            <option value='blank'>  </option>
                                            <option value='0'>0</option>
                                            <option value='1'>1</option>
                                            <option value='2'>2</option>
                                            <option value='3'>3</option>
                                            <option value='4'>4</option>
                                            <option value='5'>5</option>
                                            <option value='6'>6</option>
                                            <option value='7'>7</option>
                                            <option value='8'>8</option>
                                            <option value='9'>9</option>
                                            <option value='10'>10</option>
                                            <option value='11'>11</option>
                                            <option value='12'>12</option>
                                            <option value='13'>13</option>
                                            <option value='14'>14</option>
                                            <option value='15'>15</option>
                                            <option value='16'>16</option>
                                            <option value='17'>17</option>
                                            <option value='18'>18</option>
                                            <option value='19'>19</option>
                                            <option value='20'>20</option>
                                            <option value='21'>21</option>
                                            <option value='22'>22</option>
                                            <option value='23'>23</option>
                                            <option value='24'>24</option>
                                            <option value='25'>25</option>
                                            <option value='26'>26</option>
                                            <option value='27'>27</option>
                                            <option value='28'>28</option>
                                            <option value='29'>29</option>
                                            <option value='30'>30</option>
                                            <option value='31'>31</option>
                                            <option value='32'>32</option>
                                            <option value='33'>33</option>
                                            <option value='34'>34</option>
                                            <option value='35'>35</option>
                                            <option value='36'>36</option>
                                            <option value='37'>37</option>
                                            <option value='38'>38</option>
                                            <option value='39'>39</option>
                                            <option value='40'>40</option>
                                            <option value='41'>41</option>
                                            <option value='42'>42</option>
                                            <option value='43'>43</option>
                                            <option value='44'>44</option>
                                            <option value='45'>45</option>
                                            <option value='46'>46</option>
                                            <option value='47'>47</option>
                                            <option value='48'>48</option>
                                            <option value='49'>49</option>
                                            <option value='50'>50</option>
                                            <option value='51'>51</option>
                                            <option value='52'>52</option>
                                            <option value='53'>53</option>
                                            <option value='54'>54</option>
                                            <option value='55'>55</option>
                                            <option value='56'>56</option>
                                            <option value='57'>57</option>
                                            <option value='58'>58</option>
                                            <option value='59'>59</option>
                                        </select>

                                    </td>
                                </tr>
                                <tr>
                                    <td>End</td>
                                    <td>:</td>
                                    <td>
                                        <select name='year2' id='year2'>
                                            <option value='blank'>  </option>
                                            <option value='2012'>2012</option>
                                            <option value='2013'>2013</option>
                                            <option value='2014'>2014</option>
                                        </select>
                                        /
                                        <select name='month2' id='month2'>
                                            <option value='blank'>  </option>
                                            <option value='01'>Jan</option>
                                            <option value='02'>Feb</option>
                                            <option value='03'>Mar</option>
                                            <option value='04'>Apr</option>
                                            <option value='05'>May</option>
                                            <option value='06'>Jun</option>
                                            <option value='07'>Jul</option>
                                            <option value='08'>Aug</option>
                                            <option value='09'>Sep</option>
                                            <option value='10'>Oct</option>
                                            <option value='11'>Nov</option>
                                            <option value='12'>Dec</option>
                                        </select>
                                        /
                                        <select name='day2' id='day2'>
                                            <option value='blank'>  </option>
                                            <option value='01'>1</option>
                                            <option value='02'>2</option>
                                            <option value='03'>3</option>
                                            <option value='04'>4</option>
                                            <option value='05'>5</option>
                                            <option value='06'>6</option>
                                            <option value='07'>7</option>
                                            <option value='08'>8</option>
                                            <option value='09'>9</option>
                                            <option value='10'>10</option>
                                            <option value='11'>11</option>
                                            <option value='12'>12</option>
                                            <option value='13'>13</option>
                                            <option value='14'>14</option>
                                            <option value='15'>15</option>
                                            <option value='16'>16</option>
                                            <option value='17'>17</option>
                                            <option value='18'>18</option>
                                            <option value='19'>19</option>
                                            <option value='20'>20</option>
                                            <option value='21'>21</option>
                                            <option value='22'>22</option>
                                            <option value='23'>23</option>
                                            <option value='24'>24</option>
                                            <option value='25'>25</option>
                                            <option value='26'>26</option>
                                            <option value='27'>27</option>
                                            <option value='28'>28</option>
                                            <option value='29'>29</option>
                                            <option value='30'>30</option>
                                            <option value='31'>31</option>
                                        </select>
                                        &nbsp;
                                        <select name='hour2' id='hour2'>
                                            <option value='blank'>  </option>
                                            <option value='0'>0</option>
                                            <option value='1'>1</option>
                                            <option value='2'>2</option>
                                            <option value='3'>3</option>
                                            <option value='4'>4</option>
                                            <option value='5'>5</option>
                                            <option value='6'>6</option>
                                            <option value='7'>7</option>
                                            <option value='8'>8</option>
                                            <option value='9'>9</option>
                                            <option value='10'>10</option>
                                            <option value='11'>11</option>
                                            <option value='12'>12</option>
                                            <option value='13'>13</option>
                                            <option value='14'>14</option>
                                            <option value='15'>15</option>
                                            <option value='16'>16</option>
                                            <option value='17'>17</option>
                                            <option value='18'>18</option>
                                            <option value='19'>19</option>
                                            <option value='20'>20</option>
                                            <option value='21'>21</option>
                                            <option value='22'>22</option>
                                            <option value='23'>23</option>
                                        </select>
                                        :
                                        <select name='minute2' id='minute2'>
                                            <option value='blank'>  </option>
                                            <option value='0'>0</option>
                                            <option value='1'>1</option>
                                            <option value='2'>2</option>
                                            <option value='3'>3</option>
                                            <option value='4'>4</option>
                                            <option value='5'>5</option>
                                            <option value='6'>6</option>
                                            <option value='7'>7</option>
                                            <option value='8'>8</option>
                                            <option value='9'>9</option>
                                            <option value='10'>10</option>
                                            <option value='11'>11</option>
                                            <option value='12'>12</option>
                                            <option value='13'>13</option>
                                            <option value='14'>14</option>
                                            <option value='15'>15</option>
                                            <option value='16'>16</option>
                                            <option value='17'>17</option>
                                            <option value='18'>18</option>
                                            <option value='19'>19</option>
                                            <option value='20'>20</option>
                                            <option value='21'>21</option>
                                            <option value='22'>22</option>
                                            <option value='23'>23</option>
                                            <option value='24'>24</option>
                                            <option value='25'>25</option>
                                            <option value='26'>26</option>
                                            <option value='27'>27</option>
                                            <option value='28'>28</option>
                                            <option value='29'>29</option>
                                            <option value='30'>30</option>
                                            <option value='31'>31</option>
                                            <option value='32'>32</option>
                                            <option value='33'>33</option>
                                            <option value='34'>34</option>
                                            <option value='35'>35</option>
                                            <option value='36'>36</option>
                                            <option value='37'>37</option>
                                            <option value='38'>38</option>
                                            <option value='39'>39</option>
                                            <option value='40'>40</option>
                                            <option value='41'>41</option>
                                            <option value='42'>42</option>
                                            <option value='43'>43</option>
                                            <option value='44'>44</option>
                                            <option value='45'>45</option>
                                            <option value='46'>46</option>
                                            <option value='47'>47</option>
                                            <option value='48'>48</option>
                                            <option value='49'>49</option>
                                            <option value='50'>50</option>
                                            <option value='51'>51</option>
                                            <option value='52'>52</option>
                                            <option value='53'>53</option>
                                            <option value='54'>54</option>
                                            <option value='55'>55</option>
                                            <option value='56'>56</option>
                                            <option value='57'>57</option>
                                            <option value='58'>58</option>
                                            <option value='59'>59</option>
                                        </select>

                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><input type="submit" name="Submit" value="Work!"></td>
                                </tr>
                                <?php
                                if (isset($_SESSION['worktime_fail'])) {
                                    unset($_SESSION['worktime_fail']);
                                    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td><font color=#FF0000>Failure</td></tr>";
                                } if (isset($_SESSION['worktime_pass'])) {
                                    unset($_SESSION['worktime_pass']);
                                    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td><font color=#00FF00>Work Added</td></tr>";
                                }
                                ?>
                            </table>
                        </td>
                    </form>
                    </tr>
                </table>
            </div>
        </body>
    </center>
</html>
