<?php
session_start();
if (!(isset($_SESSION["myusername"]) && isset($_SESSION["employee_login"]))) {
    header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Employee Login</title>
        <link rel="stylesheet" type="text/css" href="../frame.css" />
    </head>

    <body>
    <center>
        <div id="header"></div>
        <div id="buttons">
            <div class="button">
                <a href="menu.php">Menu</a>
            </div>
            <div class="button">
                <a href="orders.php">Deliveries</a>
            </div>
            <div class="button">
                <a href='vehicles.php'>Change Vehicles</a>
            </div>
            <div class="button">
                <a href='worktime.php'>Record Work Time</a>
            </div>
            <div class="button">
                <a href='logout.php'>Logout</a>
            </div>
        </div>
        <?php
        ob_start();
        mysql_connect("localhost", "semsc", "semsc") or die("cannot connect");
        mysql_select_db("pizza") or die("cannot select DB");

        $sql = "Select vehicle.* from drives, vehicle where drives.Driver='" . $_SESSION['myusername'] . "' and vehicle.License_Plate=drives.License_Plate";
        // echo "<br>$sql<br>";
        $result = mysql_query($sql);
        $count = mysql_num_rows($result);

        if ($count == 0) {
            echo "You have no cars in use.<br>";
        } else {
            echo "<form name='form1' method='post' action='update_vehicles.php'><table class='table1' width='820'>";
            echo "<tr><td colspan='4'><strong>Cars Checked:</strong></td></tr>";
            echo "<tr><td>Remove</td><td>License Plate</td><td>Company Owned</td><td>MPG</td></tr>";
            while ($row = mysql_fetch_array($result)) {
                echo "<tr>";
                echo "<td><input name='checks[]' value='" . $row['License_Plate'] . "' type='checkbox' /></td>";
                echo "<td>" . $row['License_Plate'] . "</td>";
                if ($row['Company_Owned']) {
                    echo "<td>Yes</td>";
                } else {
                    echo "<td>No</td>";
                }
                echo "<td>" . $row['MPG'] . "</td>";
                echo "</tr>";
            }
            echo "<td><input type='submit' name='car' value='Remove'></td>";
            echo "</table></form>";
        }

        $arr = array();
        $arr2 = array();
        $i = 0;
        $sql = "Select * from drives where Driver='" . $_SESSION['myusername'] . "'";
        $result = mysql_query($sql);
        while ($row = mysql_fetch_array($result)) {
            $arr[$i] = $row['License_Plate'];
            $i = $i + 1;
        }
        $sql = "Select * from vehicle";
        $result = mysql_query($sql);
        $i = 0;
        while ($row = mysql_fetch_array($result)) {
            $arr2[$i] = $row['License_Plate'];
            $i = $i + 1;
        }
        $arrresult = array_diff($arr2, $arr);
        $count = count($arrresult);

        $sql = "Select * from vehicle";
        $result = mysql_query($sql);

        if ($count == 0) {
            echo "There are no cars avaliable.<br>";
        } else {
            echo "<form name='form2' method='post' action='update_vehicles.php'><table class='table1' width='820'>";
            echo "<tr><td colspan='4'><strong>Cars Checked:</strong></td></tr>";
            echo "<tr><td>Add</td><td>License Plate</td><td>Company Owned</td><td>MPG</td></tr>";
            foreach ($arrresult as $plate) {
                $sql = "Select * from vehicle where License_Plate='$plate'";
                $row = mysql_fetch_array(mysql_query($sql));
                echo "<tr>";
                echo "<td><input name='checks[]' value='" . $row['License_Plate'] . "' type='checkbox' /></td>";
                echo "<td>" . $row['License_Plate'] . "</td>";
                if ($row['Company_Owned']) {
                    echo "<td>Yes</td>";
                } else {
                    echo "<td>No</td>";
                }
                echo "<td>" . $row['MPG'] . "</td>";
                echo "</tr>";
            }
            echo "<td><input type='submit' name='car' value='Add'></td>";
            echo "</table></form>";
        }


        ob_end_flush();
        ?>

    </center>
</body>

</html>
