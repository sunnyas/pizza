<?php
session_start();
if (!(isset($_SESSION["myusername"]) && isset($_SESSION["customer_login"]))) {
    header("location:main_login.php");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link rel="stylesheet" type="text/css" href="frame.css" />
        <script src="form_functions.js" type="text/javascript"></script>
        <script type="text/javascript">
            function onAccount(){
                var pass = document.getElementById("mypassword");
                var first = document.getElementById("firstname");
                var last = document.getElementById("lastname");
                var credit = document.getElementById("creditcard");
                var address = document.getElementById("address");
                if( lengthRestriction(pass, 2, 20) 
                    && lengthRestriction(first, 2, 20) 
                    && lengthRestriction(last, 2, 20) 
                    && lengthRestriction(credit, 12, 19) 
                    && lengthRestriction(address, 2, 35) 
                    && loginSensitive(pass,"Invalid Password")
                    && loginSensitive(first,"Invalid First")
                    && loginSensitive(last,"Invalid Last")
                    && isNumeric(credit,"Invalid Credit Card #")){
                    document.forms["form1"].submit();
                }
            }
        </script>
    </head>
    <center>
        <body>
            <div id ="header">

            </div>

            <div id="buttons">
                <div class="button">
                    <a href="index.php">Home</a>
                </div>
                <div class="button">
                    <a href="cart.php">Cart</a>
                </div>
                <div class="button">
                    <a href="order.php">Order</a>
                </div>
                <div class="button">
                    <?php
                    if (isset($_SESSION["myusername"])) {
                        echo "<a href='account.php'>Account</a>";
                    } else {
                        echo "<a href='register.php'>Register</a>";
                    }
                    ?>
                </div>
                <div class="button">
                    <?php
                    if (isset($_SESSION["myusername"])) {
                        echo "<a href='logout.php'>Logout</a>";
                    } else {
                        echo "<a href='main_login.php'>Login</a>";
                    }
                    ?>
                </div>
            </div>

            <div id="login">
                <table width="300" border="0" align="center" cellpadding="0" cellspacing="1">
                    <tr>
                    <form name="form1" method="post" action="checkupdate.php">
                        <td>
                            <?php
                            ob_start();
                            $host = "localhost"; // Host name 
                            $username = "semsc"; // Mysql username 
                            $password = "semsc"; // Mysql password 
                            $db_name = "pizza"; // Database name 
                            $tbl_name = "customer"; // Table name
// Connect to server and select databse.
                            mysql_connect($host, $username, $password) or die("cannot connect");
                            mysql_select_db($db_name) or die("cannot select DB");

                            $sql = "SELECT * FROM $tbl_name WHERE username='" . $_SESSION["myusername"] . "'";
                            $result = mysql_query($sql);
                            $row = mysql_fetch_array($result);
                            echo "<table width='100%' border='0' cellpadding='3' cellspacing='1'>
                            <tr>
                                <td colspan='3'><strong>Member Update</strong></td>
                            </tr>";
                            echo "<tr><td width='78'>Username</td><td width='6'>:</td><td width='294'>" . $row['Username'] . "</td></tr>";
                            echo "<tr><td>Password</td><td>:</td><td><input name='mypassword' id='mypassword' type='password' value=" . $row['Password'] . " /></td></tr>";
                            echo "<tr><td>First Name</td><td>:</td><td><input name='firstname' id='firstname' type='text' value=" . $row['Name_First'] . " /></td></tr>";
                            echo "<tr><td>Last Name</td><td>:</td><td><input name='lastname' id='lastname' type='text' value=" . $row['Name_Last'] . " /></td></tr>";
                            echo "<tr><td>Address</td><td>:</td><td><input name='address' id='address' type='text' value=" . substr($row['Address'], 0, -3) . " /></td></tr>";
                            echo "<tr>
                                    <td>State</td>
                                    <td>:</td>
                                    <td><select name='state' id='state'>";
                            if (substr($row['Address'], -2) == 'MD') {
                                echo "<option value='MD'>MD</option>
                                            <option value='VA'>VA</option>";
                            } else {
                                echo "<option value='VA'>VA</option>
                                      <option value='MD'>MD</option>";
                            }
                            echo "</select></td>
                                </tr>";
                            echo "<tr><td>Credit Card</td><td>:</td><td><input name='creditcard' id='creditcard' type='text' value=" . $row['Credit_Card'] . " /></td></tr>";
                            if ($row['Active']) {
                                echo "<tr><td>Active</td><td>:</td><td><select name='active'><option value='1'>True</option><option value='0'>False</option></select>";
                            } else {
                                echo "<tr><td>Active</td><td>:</td><td><select name='active'><option value='0'>False</option><option value='1'>True</option></select>";
                            }
                            if (isset($_SESSION['update_failed'])) {
                                echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td><font color=#FF0000>Update Failed</td></tr>";
                                unset($_SESSION['update_failed']);
                            } else if (isset($_SESSION['update_success'])) {
                                echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td><font color=#00FF00>Update Worked</td></tr>";
                                unset($_SESSION['update_success']);
                            }
                            ob_end_flush();
                            ?>
                        <tr>
                            <td colspan='3'><button type='button' onclick="javascript:onAccount();">Update</button></td>
                        </tr>
                        </td>
                    </form>
                    </tr>
                </table>
            </div>
        </body>
    </center>
</html>
