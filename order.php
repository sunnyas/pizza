<?php
session_start();
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Order</title>
        <link rel="stylesheet" type="text/css" href="frame.css" />
    </head>
    <center>
        <body>
            <div id ="header">

            </div>

            <div id="buttons">
                <div class="button">
                    <a href="index.php">Home</a>
                </div>
                <div class="button">
                    <a href="cart.php">Cart</a>
                </div>
                <div class="button">
                    <a href="order.php">Order</a>
                </div>
                <div class="button">
                    <?php
                    if (isset($_SESSION["myusername"]) && isset($_SESSION["customer_login"])) {
                        echo "<a href='account.php'>Account</a>";
                    } else {
                        echo "<a href='register.php'>Register</a>";
                    }
                    ?>
                </div>
                <div class="button">
                    <?php
                    if (isset($_SESSION["myusername"]) && isset($_SESSION["customer_login"])) {
                        echo "<a href='logout.php'>Logout</a>";
                    } else {
                        echo "<a href='main_login.php'>Login</a>";
                    }
                    ?>
                </div>
            </div>


            <form name='form1' method='post' action='take_order.php'>
                <?php
                session_start();
                ob_start();
                echo "<table id='menu' class='table1'>";
                echo "<tr>";
                echo "<td>"; //begin premade td

                $host = "localhost"; // Host name 
                $username = "semsc"; // Mysql username 
                $password = "semsc"; // Mysql password 
                $db_name = "pizza"; // Database name 
                // Connect to server and select databse.
                mysql_connect($host, $username, $password) or die("cannot connect");
                mysql_select_db($db_name) or die("cannot select DB");

                $tbl_name = "pizza";
                $sql = "SELECT * FROM $tbl_name where Name_Pizza is not null";
                $result = mysql_query($sql);

                //insert into pizza(Pizza_ID, Width, Happy_Hour, Crust, Price) 
                print("Our Premade Pizzas: <br>");
                echo "<select name='premade_pizza'>
                <option value='Custom'>Custom</option>";
                while ($row = mysql_fetch_array($result)) {
                    $sql = "SELECT Time_start FROM happy_hour where Pizza='" . $row['Pizza_ID'] . "'";
                    $result2 = mysql_query($sql);
                    $hh = false;
                    while ($row2 = mysql_fetch_array($result2)) {
                        $time1 = strtotime($row2[0]);
                        $diff = strtotime("now") - $time1;
                        if ($diff >= 0 && $diff < 3600) {
                            $hh = true;
                        }
                    }
                    if ($hh) {
                        echo "<option value='" . $row['Name_Pizza'] . "'>" . $row['Name_Pizza']
                        . " (" . $row['Width'] . " inch diameter, " . $row['Crust']
                        . " crust. $" . $row['Price'] . ") (HH)</option>";
                    } else {
                        echo "<option value='" . $row['Name_Pizza'] . "'>" . $row['Name_Pizza']
                        . " (" . $row['Width'] . " inch diameter, " . $row['Crust']
                        . " crust. $" . $row['Price'] . ")</option>";
                    }
                }

                echo "</select></td>"; //end premade td

                if ((isset($_SESSION["myusername"]) && isset($_SESSION["customer_login"]))) {


                    echo "<td>Pizza Quantity:<br> 
    <select name='numpizza' id='numpizza'>
      <option value='0'>0</option>
      <option value='1'>1</option>
      <option value='2'>2</option>
      <option value='3'>3</option>
      <option value='4'>4</option>
      <option value='5'>5</option>
      <option value='6'>6</option>
      <option value='7'>7</option>
      <option value='8'>8</option>
      <option value='9'>9</option>
    </select>
    
    <input type='submit' name='Submit' value='Order'>
    </td></tr>";
                } else {
                    echo "<td>Login to Order!</td>";
                }

                echo "<tr><td rowspan='2'>"; //begin topping td

                $result = mysql_query("SELECT * FROM topping");
                echo "<table id='toppings' class='table1'>";
                echo "<tr><td colspan='3'>Custom Only</td><tr>";
                echo "<tr><td></td><td>Topping</td><td>Cost</tr>";

                while ($row = mysql_fetch_array($result)) {
                    echo "<tr>";
                    echo "<td><input type='checkbox' name='checks[]' 
                        value='" . $row['Name_Topping'] . "'></td><td>" . $row['Name_Topping'] . "</td>";
                    echo "<td>" . $row['Cost'] . "</td>";
                    echo "</tr>";
                }
                echo "</table>";

                echo "</td>"; //end topping td
                echo "<td>"; //begin crust/size td

                echo "<table><tr><td>"; //interior crust/size table

                echo "<table id='crusts' class='table1'>";
                echo "<tr><td colspan='3'>Custom Crust</td></tr>";
                echo "<tr><td>";
                echo "<input type='radio' value'normal' name='crust' checked='yes'></td><td>Normal</td></tr><tr><td>
            <input type='radio' value='thin' name='crust'></td><td>Thin</td></tr><tr><td>
            <input type='radio' value='stuffed' name='crust'></td><td>Stuffed";
                echo "</td>";
                echo "</tr>";
                echo "</table>";

                echo "</td><td>"; //interior crust/size table

                echo "<table id='sizes' class='table1'>";
                echo "<tr><td colspan='3'>Custom Size</td></tr>";
                echo "<tr><td>";
                echo "<input type='radio' value='10' name='size'></td><td>Small (10\")</td></tr><tr><td>
            <input type='radio' value='12' name='size' checked='yes'></td><td>Normal (12\")</td></tr><tr><td>
            <input type='radio' value='14' stuffed' name='size'></td><td>Large (14\")";
                echo "</td>";
                echo "</tr>";
                echo "</table>";

                echo "</td></tr></table>"; //end interior crust/size table


                echo "</td></tr>"; //end crust/size td
                echo "<tr><td>"; //begin drink td

                $result = mysql_query("SELECT * FROM drink");
                echo "<table id='drinks' class='table1'>";
                echo "<tr><td>#</td><td>Drink</td><td>Cost</tr>";
                while ($row = mysql_fetch_array($result)) {
                    echo "<tr>";

                    echo "<td><select name=\"drink['" . $row['Drink_Name'] . "']\" id='" . $row['Drink_Name'] . "'>
          <option value='0'>0</option>
          <option value='1'>1</option>
          <option value='2'>2</option>
          <option value='3'>3</option>
          <option value='4'>4</option>
          <option value='5'>5</option>
          <option value='6'>6</option>
          <option value='7'>7</option>
          <option value='8'>8</option>
          <option value='9'>9</option>
        </select></td>";

                    echo "<td>" . $row['Drink_Name'] . ": " . $row['Description'] . "</td>";
                    echo "<td>" . $row['Cost'] . "</td>";
                    echo "</tr>";
                }
                echo "</table>";

                echo "</td>"; //end drink td
                echo "</tr>";
                echo "</table>";




                ob_end_flush();
                ?>
            </form>

        </body>
    </center>
</html>





