<?php
session_start();
if ((isset($_SESSION["myusername"]) && isset($_SESSION["customer_login"]))) {
    header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link rel="stylesheet" type="text/css" href="frame.css" />
        <script src="form_functions.js" type="text/javascript"></script>
    </head>
    <center>
        <body>
            <div id ="header">

            </div>

            <div id="buttons">
                <div class="button">
                    <a href="index.php">Home</a>
                </div>
                <div class="button">
                    <a href="cart.php">Cart</a>
                </div>
                <div class="button">
                    <a href="order.php">Order</a>
                </div>
                <div class="button">
                    <?php
                    if (isset($_SESSION["myusername"]) && isset($_SESSION["customer_login"])) {
                        echo "<a href='account.php'>Account</a>";
                    } else {
                        echo "<a href='register.php'>Register</a>";
                    }
                    ?>
                </div>
                <div class="button">
                    <?php
                    if (isset($_SESSION["myusername"]) && isset($_SESSION["customer_login"])) {
                        echo "<a href='logout.php'>Logout</a>";
                    } else {
                        echo "<a href='main_login.php'>Login</a>";
                    }
                    ?>
                </div>
            </div>

            <div id="login">
                <table width="300" border="0" align="center" cellpadding="0" cellspacing="1">
                    <tr>
                    <form name="form1" method="post" action="checklogin.php">
                        <td>
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td colspan="3"><strong>Member Login </strong></td>
                                </tr>
                                <tr>
                                    <td width="78">Username</td>
                                    <td width="6">:</td>
                                    <td width="294"><input name="myusername" type="text" id="myusername"></td>
                                </tr>
                                <tr>
                                    <td>Password</td>
                                    <td>:</td>
                                    <td><input name="mypassword" type="password" id="mypassword"></td>
                                </tr>
                                <tr>
                                    <td colspan='3'><button type='button' onclick="javascript:submitLogin();">Login</button></td>
                                </tr>
                                <?php
                                if (isset($_SESSION['login_fail'])) {
                                    unset($_SESSION['login_fail']);
                                    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td><font color=#FF0000>Login Failed</td></tr>";
                                }
                                ?>
                            </table>
                        </td>
                    </form>
                    </tr>
                </table>
            </div>


        </body>
    </center>
</html>
