<?php

session_start();
if (!(isset($_SESSION["myusername"]) && isset($_SESSION["customer_login"]))) {
    header("location:main_login.php");
} else {
    ob_start();

    $host = "localhost";
    $username = "semsc";
    $password = "semsc";
    $db_name = "pizza";

    mysql_connect($host, $username, $password) or die("cannot connect");
    mysql_select_db($db_name) or die("cannot select DB");

    $sql = "SELECT * FROM orders WHERE Customer='" . $_SESSION['myusername'] . "' and Received is null";
    $result = mysql_query($sql);
    $count = mysql_num_rows($result);

    if (count == 0) {
        $sql = "INSERT INTO orders (Customer) VALUES ('" . $_SESSION['myusername'] . "')";
        mysql_query($sql) or die(mysql_error());
    }

    $sql = "SELECT * FROM orders WHERE Customer='" . $_SESSION['myusername'] . "' and Received is null";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    $order_id = $row['Order_ID'];

    if ($_POST['numpizza'] > 0) {
        if ($_POST['premade_pizza'] == 'Custom') {
            $sql = "INSERT INTO pizza (Crust,Width) VALUES ('" . $_POST['crust'] . "','" . $_POST['size'] . "')";
            mysql_query($sql) or die(mysql_error());

            $sql = "SELECT * FROM pizza WHERE Crust='" . $_POST['crust'] . "' and Width='" . $_POST['size'] . "' and Price='0'";
            //var_dump($_POST);
            //var_dump($sql);
            $result = mysql_query($sql);
            $row = mysql_fetch_array($result);
            $pizza_id = $row['Pizza_ID'];

            $sql = "INSERT INTO contains_solids (Order_ID,Pizza_ID,Quantity) VALUES ('$order_id','$pizza_id','" . $_POST['numpizza'] . "')";
            mysql_query($sql) or die(mysql_error());

            //$_POST['numpizza']; //Quantity of pizza
            //$_POST['size']; //Width in int
            //$_SESSION['Crust']; //Crust in char
            $topping_price = 0;
            $topping_calories = 0; //Calories

            $sql = "SELECT * FROM topping";
            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {
                foreach ($_POST['checks'] as $value) {
                    if ($row['Name_Topping'] == $value) {
                        // value is name_topping, add to Contains table
                        $sql = "INSERT INTO contain (Pizza_ID,Topping_Name) VALUES ('$pizza_id','$value')";
                        mysql_query($sql) or die(mysql_error());
                        $topping_price = $topping_price + $row['Cost'];
                        $topping_calories = $topping_calories + $row['Calories'];
                    }
                }
            }

            $pizza_price = round((10 + $topping_price) * $_POST['size'] / 10.0, 2);
            $pizza_calories = $topping_calories;

            $sql = "UPDATE pizza SET Calories='$pizza_calories', Price='$pizza_price' WHERE Pizza_ID='$pizza_id'";
            mysql_query($sql) or die(mysql_error());
            //Insert row, then select row to get pizza ID and add it to Contains_Solid
        } else {
            $sql = "SELECT * FROM pizza where Name_Pizza is not null";
            $result = mysql_query($sql);
            while ($row = mysql_fetch_array($result)) {
                if ($row['Name_Pizza'] == $_POST['premade_pizza']) {
                    $pizza_id = $row['Pizza_ID']; //ID for pizza
                    $sql = "SELECT Quantity FROM contains_solids where Pizza_ID='$pizza_id' and Order_ID='$order_id'";
                    $result2 = mysql_query($sql);
                    if (mysql_num_rows($result2) > 0) {
                        $row2 = mysql_fetch_array($result2);
                        $quantity = $row2[0] + $_POST['numpizza'];
                        $sql = "UPDATE contains_solids SET Quantity='$quantity' WHERE Pizza_ID='$pizza_id' and Order_ID='$order_id'";
                        mysql_query($sql) or die(mysql_error());
                    } else {
                        $sql = "INSERT INTO contains_solids (Order_ID,Pizza_ID,Quantity) VALUES ('$order_id','$pizza_id','" . $_POST['numpizza'] . "')";
                        mysql_query($sql) or die(mysql_error());
                    }
                }
            }
        }
    }
    //var_dump($_POST['drink']);
    foreach ($_POST['drink'] as $name => $num) {
        //echo "\n$name => $num";
        if ($num > 0) {
            $sql = "SELECT Quantity FROM contains_fluids where Drink_Name=$name and Order_ID='$order_id'";
            $result2 = mysql_query($sql);
            $count = mysql_num_rows($result2);
            if ($count > 0) {
                $row2 = mysql_fetch_array($result2);
                $quantity = $row2[0] + $num;
                $sql = "UPDATE contains_fluids SET Quantity='$quantity' where Drink_Name=$name and Order_ID='$order_id'";
                mysql_query($sql) or die(mysql_error());
            } else {
                $sql = "INSERT INTO contains_fluids (Order_ID,Drink_Name,Quantity) VALUES ('$order_id',$name,'$num')";
                mysql_query($sql) or die(mysql_error());
                //Drink detail in row.
            }
        }
    }

    header("location:cart.php");
    ob_end_flush();
}
?>