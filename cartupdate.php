<?php
session_start();
if (!(isset($_SESSION["myusername"]) && isset($_SESSION["customer_login"]))) {
    header("location:main_login.php");
} elseif ($_POST['order'] == 'Delete') {
    ob_start();
    mysql_connect("localhost", "semsc", "semsc") or die("cannot connect");
    mysql_select_db("pizza") or die("cannot select DB");

    foreach ($_POST['pizzas'] as $order_id => $value) {
        foreach ($value as $pizza_id => $bool) {
            $order_id = $order_id + 0;
            $pizza_id = $pizza_id + 0;
            $sql = "DELETE FROM contains_solids WHERE Order_ID='$order_id' and Pizza_ID='$pizza_id'";
            mysql_query($sql) or die(mysql_error());
        }
    }

    foreach ($_POST['drinks'] as $order_id => $value) {
        foreach ($value as $drink_name => $bool) {
            $order_id = $order_id + 0;
            $sql = "DELETE FROM contains_fluids WHERE Order_ID='$order_id' and Drink_Name='$drink_name'";
            mysql_query($sql) or die(mysql_error());
        }
    }

    header("location:cart.php");

    ob_end_flush();
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bill</title>
        <link rel="stylesheet" type="text/css" href="frame.css" />
    </head>
    <center>
        <body>
            <div id ="header">

            </div>

            <div id="buttons">
                <div class="button">
                    <a href="index.php">Home</a>
                </div>
                <div class="button">
                    <a href="cart.php">Cart</a>
                </div>
                <div class="button">
                    <a href="order.php">Order</a>
                </div>
                <div class="button">
                    <?php
                    if (isset($_SESSION["myusername"])) {
                        echo "<a href='account.php'>Account</a>";
                    } else {
                        echo "<a href='register.php'>Register</a>";
                    }
                    ?>
                </div>
                <div class="button">
                    <?php
                    if (isset($_SESSION["myusername"])) {
                        echo "<a href='logout.php'>Logout</a>";
                    } else {
                        echo "<a href='main_login.php'>Login</a>";
                    }
                    ?>
                </div>
            </div>
            <?php
            if ($_POST['order'] == 'Order') {
                ob_start();
                mysql_connect("localhost", "semsc", "semsc") or die("cannot connect");
                mysql_select_db("pizza") or die("cannot select DB");
                $order_id = $_SESSION['order_id'];
                $total_liquids = $_SESSION['total_liq'];
                $total_solids = $_SESSION['total_sol'];
                $total_tax = $_SESSION['total_tax'];
                $total = $total_liquids + $total_solids + $total_tax;
                $received = new DateTime("now");
                $received = $received->format("Y-m-d H:i:s");
                $pay_type = $_POST['payment'];
                $distance = rand(0, 30);
                $expected = new DateTime("now");
                $expected->add(date_interval_create_from_date_string("+" . ($distance * 2) . " minutes"));
                $expected = $expected->format("Y-m-d H:i:s");

                $sql_bill = "UPDATE orders 
                SET Price='$total', Price_solids='$total_solids', Price_fluids='$total_liquids', Expected='$expected', Received='$received', Payment_type='$pay_type'
                WHERE Order_ID='$order_id'";

                $state = $_SESSION['state'];
                $total_solids = 0;
                $total_liquids = 0;
                $total_tax = 0;

                $sql = "Select orders.Order_ID, pizza.Pizza_ID, pizza.Name_Pizza, contains_solids.Quantity, pizza.Price
                from orders, pizza, contains_solids
                where orders.Received is null
                and orders.Order_ID=contains_solids.Order_ID
                and contains_solids.Pizza_ID=pizza.Pizza_ID
                and orders.Customer='" . $_SESSION["myusername"] . "'";
                $result = mysql_query($sql);

                if (mysql_num_rows($result) != 0) {
                    echo "<table class='table1' width='820'>";
                    echo "<tr><td colspan='5'>Bill for Order #$order_id</td></tr>";
                    echo "<tr><td>#</td><td>Name</td><td>Quantity</td><td>Price</td><td>Total</td></tr>";
                    $i = 1;
                    while ($row = mysql_fetch_array($result)) {
                        $order_id = $row['Order_ID'];
                        echo "<tr>";
                        echo "<td>$i</td>";
                        $i = $i + 1;
                        if (is_null($row['Name_Pizza'])) {
                            $sql = "select Topping_Name from contain where Pizza_ID='" . $row['Pizza_ID'] . "'";
                            $result2 = mysql_query($sql);
                            echo "<td>Custom Pizza:&nbsp;";
                            $count = mysql_num_rows($result2);
                            if ($count >= 1) {
                                $row2 = mysql_fetch_array($result2);
                                echo "<br>$row2[0]";
                                while ($row2 = mysql_fetch_array($result2)) {
                                    echo ", $row2[0]";
                                }
                            } else {
                                echo "No Toppings";
                            }
                            echo "</td><td>" . $row['Quantity'] . "</td>";
                            echo "<td>" . $row['Price'] . "</td>";
                            $total = $row['Quantity'] * $row['Price'];
                            echo "<td>$total</td>";
                            $total_solids = $total_solids + $total;
                        } else {
                            echo "<td>" . $row['Name_Pizza'] . "</td>";
                            echo "<td>" . $row['Quantity'] . "</td>";
                            echo "<td>" . $row['Price'] . "</td>";
                            $sql = "Select Time_start from happy_hour where Pizza='" . $row['Pizza_ID'] . "'";
                            $result2 = mysql_query($sql);
                            $total = $row['Quantity'] * $row['Price'];
                            $hh = false;
                            while ($row2 = mysql_fetch_array($result2)) {
                                $time1 = strtotime($row2[0]);
                                $diff = strtotime("now") - $time1;
                                if ($diff >= 0 && $diff < 3600) {
                                    $total = ceil($row['Quantity'] / 2.0) * $row['Price'];
                                    $hh = true;
                                }
                            }
                            if ($hh) {
                                echo "<td>$total (HH)</td>";
                            } else {
                                echo "<td>$total</td>";
                            }
                            $total_solids = $total_solids + $total;
                        }
                        echo "</tr>";
                    }

                    $sql = "Select orders.Order_ID, contains_fluids.Drink_Name, contains_fluids.Quantity, drink.Cost
                from orders, contains_fluids, drink
                where orders.Received is null
                and orders.Order_ID=contains_fluids.Order_ID
                and contains_fluids.Drink_Name=drink.Drink_Name
                and orders.Customer='" . $_SESSION["myusername"] . "'";
                    $result = mysql_query($sql);
                    while ($row = mysql_fetch_array($result)) {
                        $order_id = $row['Order_ID'];
                        echo "<tr>";
                        echo "<td>$i</td>";
                        $i = $i + 1;
                        echo "<td>" . $row['Drink_Name'] . "</td>";
                        echo "<td>" . $row['Quantity'] . "</td>";
                        echo "<td>" . $row['Cost'] . "</td>";
                        $total = $row['Cost'] * $row['Quantity'];
                        echo "<td>$total</td>";
                        $total_liquids = $total_liquids + $total;
                        if ($state == 'MD') {
                            $total_tax = round($total_liquids * .06, 2);
                        } else {
                            $total_tax = round($total_liquids * .025, 2);
                        }
                    }
                    echo "<tr>";
                    echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                    echo "<td>Pizzas</td><td>$total_solids</td>";
                    echo "</tr>";

                    echo "<tr>";
                    echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                    echo "<td>Drinks</td><td>$total_liquids</td>";
                    echo "</tr>";

                    echo "<tr>";
                    echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                    echo "<td>Taxes</td><td>$total_tax</td>";
                    echo "</tr>";

                    $total = $total_liquids + $total_solids + $total_tax;

                    echo "<tr>";
                    echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                    echo "<td>Total</td><td>$total</td>";
                    echo "</tr>";

                    echo "<tr>";
                    echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                    echo "<td>Paid With</td><td>$pay_type</td>";
                    echo "</tr>";

                    echo "<tr>";
                    echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                    echo "<td>Payment At</td><td>$received</td>";
                    echo "</tr>";

                    echo "<tr>";
                    echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                    echo "<td>Distance for Deliv</td><td>$distance</td>";
                    echo "</tr>";

                    echo "<tr>";
                    echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                    echo "<td>Estimated time</td><td>$expected</td>";
                    echo "</tr>";

                    echo "</table>";

                    mysql_query($sql_bill) or die(mysql_error());
                }
                ob_end_flush();
            }
            ?>
        </body>
    </center>
</html>
