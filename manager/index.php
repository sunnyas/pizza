<?php
session_start();
if (isset($_SESSION["myusername"]) && isset($_SESSION["manager_login"])) {
    header("location:menu.php");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="frame.css" />
    </head>
    <body>
        <div id ="header">

        </div>

        <div id="buttons">

        </div>

        <div id="login">
            <table width="300" border="0" align="center" cellpadding="0" cellspacing="1">
                <tr>
                <form name="form1" method="post" action="check_login.php">
                    <td>
                        <table width="100%" border="0" cellpadding="3" cellspacing="1">
                            <tr>
                                <td colspan="3"><strong>Manager Login </strong></td>
                            </tr>
                            <tr>
                                <td width="78">Username</td>
                                <td width="6">:</td>
                                <td width="294"><input name="myusername" type="text" id="myusername"></td>
                            </tr>
                            <tr>
                                <td>Password</td>
                                <td>:</td>
                                <td><input name="mypassword" type="text" id="mypassword"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><input type="submit" name="Submit" value="Login"></td>
                            </tr>
                            <?php
                            if (isset($_SESSION['login_fail'])) {
                                unset($_SESSION['login_fail']);
                                echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td><font color=#FF0000>Login Failed</td></tr>";
                            }
                            ?>
                        </table>
                    </td>
                </form>
                </tr>
            </table>
        </div>


    </body>
</html>
