<?php
session_start();
if (!(isset($_SESSION["myusername"]) && isset($_SESSION["manager_login"]))) {
//    header("location:index.php");
}
?>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="frame.css" />
  </head>
  <body>
    <div id="header"></div>
    <div id="buttons">
      <div class="button">
        <a href="index.php">Home</a>
      </div>
    </div>
    <div class='choices'>
      
      <?php
      //Time-Window Revenue: 
      //cash flow at different times: hour, day, week, etc
      //period specified by the store manager
      //provide the gross for each product category (pizza and drinks)
      //include similar figures for the same time period of the previous day, week, month, and year.        
      //Note, if the time-window is larger than 24 hours, the previous day is not meaningful. 
      //        (Similarly, if it is larger than a week, the previous week has no meaning.)
      
      mysql_connect("localhost", "semsc", "semsc") or die("cannot connect");
      mysql_select_db("pizza") or die("cannot select DB");

      //get input from user for time duration (from CashFlowGenerator.php)
      $duration = $_POST['cf_time'];
      //echo "Cash flow for the past " . $duration . ".";
      
      echo "<table id='cf_report'>
      <tr>
      <td>Time Period</td>
      <td>Revenue</td>
      <td>Pizza</td>
      <td>Drinks</td>
      </tr>";
      
      //original test block
//      $sql = "select order_id, DATE_SUB(CURRENT_DATE(), INTERVAL 2 " . $duration . ") <= orders.Paid_Time as In_Time, 
//        Price, Price_solids, Price_fluids
//        from orders";
//      $result = mysql_query($sql);
//      $price = 0;
//      $pizzas = 0;
//      $drinks = 0;
//      while($row = mysql_fetch_array($result)) {
//        if($row[1] != 0) {
//          $price += $row['Price'];
//          $pizzas += $row['Price_solids'];
//          $drinks += $row['Price_fluids'];
//        }
//      }
//      echo "<tr>
//        <td>Specific: (2)" . $duration . "</td>
//        <td>$" . $price . "</td>
//        <td>$" . $pizzas . "</td>
//        <td>$" . $drinks . "</td>
//        </tr>";
      
      if($duration =='Hour'){
      $sql = "select order_id, DATE_SUB(CURRENT_DATE(), INTERVAL 1 HOUR) <= orders.Paid_Time as In_Time, 
        Price, Price_solids, Price_fluids
        from orders";
      $result = mysql_query($sql);
      $price = 0;
      $pizzas = 0;
      $drinks = 0;
      while($row = mysql_fetch_array($result)) {
        if($row[1] != 0) {
          $price += $row['Price'];
          $pizzas += $row['Price_solids'];
          $drinks += $row['Price_fluids'];
        }
      }
      echo "<tr>
        <td>Hour</td>
        <td>$" . $price . "</td>
        <td>$" . $pizzas . "</td>
        <td>$" . $drinks . "</td>
        </tr>";
      }
      
      if($duration =='Hour' || $duration == 'Day'){
      $sql = "select order_id, DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY) <= orders.Paid_Time as In_Time, 
        Price, Price_solids, Price_fluids
        from orders";
      $result = mysql_query($sql);
      $price = 0;
      $pizzas = 0;
      $drinks = 0;
      while($row = mysql_fetch_array($result)) {
        if($row[1] != 0) {
          $price += $row['Price'];
          $pizzas += $row['Price_solids'];
          $drinks += $row['Price_fluids'];
        }
      }
      echo "<tr>
        <td>Day</td>
        <td>$" . $price . "</td>
        <td>$" . $pizzas . "</td>
        <td>$" . $drinks . "</td>
        </tr>";
      }   
      
      if($duration =='Hour' || $duration == 'Day' || $duration == 'Week'){
      $sql = "select order_id, DATE_SUB(CURRENT_DATE(), INTERVAL 1 WEEK) <= orders.Paid_Time as In_Time, 
        Price, Price_solids, Price_fluids
        from orders";
      $result = mysql_query($sql);
      $price = 0;
      $pizzas = 0;
      $drinks = 0;
      while($row = mysql_fetch_array($result)) {
        if($row[1] != 0) {
          $price += $row['Price'];
          $pizzas += $row['Price_solids'];
          $drinks += $row['Price_fluids'];
        }
      }
      echo "<tr>
        <td>Week</td>
        <td>$" . $price . "</td>
        <td>$" . $pizzas . "</td>
        <td>$" . $drinks . "</td>
        </tr>";    
      }
      
      if($duration =='Hour' || $duration == 'Day' || $duration == 'Week' || $duration == 'Month'){
      $sql = "select order_id, DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH) <= orders.Paid_Time as In_Time, 
        Price, Price_solids, Price_fluids
        from orders";
      $result = mysql_query($sql);
      $price = 0;
      $pizzas = 0;
      $drinks = 0;
      while($row = mysql_fetch_array($result)) {
        if($row[1] != 0) {
          $price += $row['Price'];
          $pizzas += $row['Price_solids'];
          $drinks += $row['Price_fluids'];
        }
      }
      echo "<tr>
        <td>Month</td>
        <td>$" . $price . "</td>
        <td>$" . $pizzas . "</td>
        <td>$" . $drinks . "</td>
        </tr>";
      }
      
      $sql = "select order_id, DATE_SUB(CURRENT_DATE(), INTERVAL 1 YEAR) <= orders.Paid_Time as In_Time, 
        Price, Price_solids, Price_fluids
        from orders";
      $result = mysql_query($sql);
      $price = 0;
      $pizzas = 0;
      $drinks = 0;
      while($row = mysql_fetch_array($result)) {
        if($row[1] != 0) {
          $price += $row['Price'];
          $pizzas += $row['Price_solids'];
          $drinks += $row['Price_fluids'];
        }
      }
      echo "<tr>
        <td>Year</td>
        <td>$" . $price . "</td>
        <td>$" . $pizzas . "</td>
        <td>$" . $drinks . "</td>
        </tr>";
      
      
      
      
      
      echo "</table>";
      ?>
    </div>
  </body>
</html>





