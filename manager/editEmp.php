<?php
session_start();
if (!(isset($_SESSION["myusername"]) && isset($_SESSION["manager_login"]))) {
    header("location:index.php");
}
?>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="frame.css" />
    <script src="../form_functions.js" type="text/javascript"></script>
    <script type="text/javascript">
      function onHire(){
        var empusername = document.getElementById("empusername");
        var emppassword = document.getElementById("emppassword");
        var name = document.getElementById("name");
        var wage = document.getElementById("wage");
        if(lengthRestriction(empusername, 2, 20) 
          && lengthRestriction(emppassword, 2, 20) 
          && lengthRestriction(name, 2, 40) 
          && loginSensitive(empusername,"Invalid Username") 
          && loginSensitive(emppassword,"Invalid Password")
          && loginSensitive(name,"Invalid Name")
          && isNumeric(wage,"Invalid Wage"))
        {
          document.forms["form_hire"].submit();
        }
      }
    </script>

  </head>
  <body>
    <div id="header"></div>
    <div id="buttons">
      <div class="button">
        <a href="index.php">Home</a>
      </div>
    </div>
    <div class='choices'>
      
  <center>
      <table  width="300" border="0" align="center" cellpadding="0" cellspacing="1">
        <tr>
        <form name="form_del" method="post" action="editEmp_work.php">
          <td>
            <table class='table1' border="1" cellpadding="3" cellspacing="1">

              <tr>
                <td colspan='3'><strong>Delete Employee:</strong></td>
              </tr>

              <tr>
                <td colspan='3'>
                  <?php
                  mysql_connect("localhost", "semsc", "semsc") or die("cannot connect");
                  mysql_select_db("pizza") or die("cannot select DB");

                  $col = array();
                  $sql = "select employee.Username from employee";
                  $result = mysql_query($sql);

                  //construct a list of employees
                  while ($row = mysql_fetch_array($result)) {
                    $col[] = $row[0];
                    //echo $row[0] . "<br>"; 
                  }
                  //echo "<br> col values: " . $col[0] . " " . $col[1];
                  //echo "<br> count: " . count($col);

                  echo "<select name='todelete' id='todelete'>
                    <option value='blank'></option>";
                  for ($i = 0; $i < count($col); $i++) {
                    echo "<option value='" . $col[$i] . "'>" . $col[$i] . "</option>";
                  }

                  echo "</select>";
                  ?>
                </td>
              </tr>

              <tr>
                <td>
                  <input type='submit' name='Submit' value='Fire'>
                </td>
              </tr>

            </table>

          </td>
        </form>


        <form name="form_hire" method="post" action="editEmp_work.php">
          <table class='table1' border="1" cellpadding="3" cellspacing="1">
            <tr>
              <td colspan='3'><strong>Hire Employee:</strong></td>
            </tr>

            <tr>
              <td width="78">Username</td>
              <td width="6">:</td>
              <td width="294"><input name="empusername" type="text" id="empusername"></td>
            </tr>
            <tr>
              <td>Password</td>
              <td>:</td>
              <td><input name="emppassword" type="password" id="emppassword"></td>
            </tr>
            <tr>
              <td>Name</td>
              <td>:</td>
              <td><input name="name" type="text" id="name"></td>
            </tr>

            <tr>
              <td>Wage</td>
              <td>:</td>
              <td><input name="wage" type="text" id="wage"></td>
            </tr>

            <tr>
              <!--<td colspan='3'><input type='submit' name='Submit' value='Hire'></td>-->
              <td colspan='3'><button type='button' onclick="javascript:onHire();">Hire</button></td>
            </tr>
            <?php
            if (isset($_SESSION['editEmp_fail'])) {
              unset($_SESSION['editEmp_fail']);
              echo "<tr><td><font color=#FF0000>Failure</td></tr>";
            } if (isset($_SESSION['editEmp_pass'])) {
              unset($_SESSION['editEmp_pass']);
              echo "<tr><td><font color=#00FF00>Employee Added</td></tr>";
            }
            ?>
          </table>
        </form>




        </tr>
      </table>

  </center>
</div>
</body>
</html>





