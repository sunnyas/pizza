<?php
session_start();
if (!(isset($_SESSION["myusername"]) && isset($_SESSION["manager_login"]))) {
    header("location:index.php");
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to C.C.'s Pizza Shop</title>
        <link rel="stylesheet" type="text/css" href="frame.css" />
    </head>
    <body>
        <div id="header"></div>
        <div id="buttons">
            <div class="button">
                <a href="menu.php">Menu</a>
            </div>
            <div class="button">
                <a href="orders.php">Deliveries</a>
            </div>
            <div class="button">
                <a href='vehicles.php'>Change Vehicles</a>
            </div>
            <div class="button">
                <a href='logout.php'>Logout</a>
            </div>
        </div>
    </body>

</html>
