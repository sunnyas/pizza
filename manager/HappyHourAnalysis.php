<?php
session_start();
if (!(isset($_SESSION["myusername"]) && isset($_SESSION["manager_login"]))) {
    header("location:index.php");
}
?>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="frame.css" />
  </head>
  <body>
    <div id="header"></div>
    <div id="buttons">
      <div class="button">
        <a href="index.php">Home</a>
      </div>
    </div>
    <div class='choices'>

      <?php
      /*
        create table happy_hour(
        Happy_ID int auto_increment primary key,
        Time_start datetime,
        Pizza int references Pizza(Pizza_ID));

        Happy Hour Analysis Report:
        for each happy hour offered, compare with:
        corresponding hours of the days it was not offered for a period of two weeks
        (one week before and one week after)
       */

      /*
        foreach happy_ID
        for (-7 to +7 days) + Time_start
        total revenue for Time_start + 1 HOUR
       */

      mysql_connect("localhost", "semsc", "semsc") or die("cannot connect");
      mysql_select_db("pizza") or die("cannot select DB");

      $sql_outer = "
        select sum(orders.Price) as Sales, happy_hour.Time_start as Start, DATE_ADD(happy_hour.Time_start, INTERVAL 1 HOUR) as End
        from orders, happy_hour
        where happy_hour.Time_start <= orders.Received
          and orders.Received <= DATE_ADD(happy_hour.Time_start, INTERVAL 1 HOUR)
        group by DAY(orders.Received)
        ";
      
      //for each happy hour that has values, generate the report
      $result_outer = mysql_query($sql_outer);
      while($row_outer = mysql_fetch_array($result_outer)) {
      //echo "For the happy hour starting on " . date($row_outer[1]) . ".";
      //echo "<br> Sales: " . $row_outer[0] . "<br>HH Start: " . $row_outer[1] . "<br>HH End: " . $row_outer[2];
      //echo "<br><br>";
      
      echo "<table width='100%' id='hh_period' border='2'>
        <tr>
        <td>One Week Before</td>
        <td>Six Days Before</td>
        <td>Five Days Before</td>
        <td>Four Days Before</td>
        <td>Three Days Before</td>
        <td>Two Days Before</td>
        <td>One Day Before</td>
        <td>Day of Happy Hour</td>
        <td>One Day After</td>
        <td>Two Days After</td>
        <td>Three Days After</td>
        <td>Four Days After</td>
        <td>Five Days After</td>
        <td>Six Days After</td>
        <td>One Week After</td>
        </tr>
        <tr><td colspan='15'>Sales for the happy hour that started " . date($row_outer[1]) . ", and the surrounding weeks.<td></tr>
        <tr>";
      for ($day = -7; $day <= 7; $day++) {
        
        $sql = "
        select sum(orders.Price) as Sales, happy_hour.Time_start as Start, DATE_ADD(happy_hour.Time_start, INTERVAL 1 HOUR) as End
        from orders, happy_hour
        where DATE_ADD(happy_hour.Time_start, INTERVAL " . $day . " DAY) <= orders.Received
          and orders.Received <= DATE_ADD(DATE_ADD(happy_hour.Time_start, INTERVAL 1 HOUR), INTERVAL " . $day . " DAY)
        group by DAY(orders.Received)
        ";
        
        //echo $sql;
        //"<br>HH Start: " . $row[1] . "<br>HH End: " . $row[2] . 
        $result = mysql_query($sql);
        $row = mysql_fetch_array($result);
 
        echo "<td>$" . (0 + $row[0]) . "</td>";
      }
      
      
      echo "</tr></table>";
      
      }
      
      ?>

    </div>
  </body>
</html>





