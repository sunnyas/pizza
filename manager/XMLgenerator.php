<?php
session_start();
if (!(isset($_SESSION["myusername"]) && isset($_SESSION["manager_login"]))) {
//    header("location:index.php");
}
?>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="frame.css" />
  </head>
  
  <body>
    <div id="header"></div>
    <div id="buttons">
      <div class="button">
        <a href="index.php">Home</a>
      </div>
    </div>
    <div class='choices'>
      <form name='xmldata' action='XMLreport.php' method='post'>

        Select Year and Month:<br> 
        <select name='xmlyear' id='xmlyear'>
          <option value='2012'>2012</option>
          <option value='2013'>2013</option>
          <option value='2014'>2014</option>
        </select>

        <select name='xmlmonth' id='xmlmonth'>
          <option value='January'>January</option>
          <option value='February'>February</option>
          <option value='March'>March</option>
          <option value='April'>April</option>
          <option value='May'>May</option>
          <option value='June'>June</option>
          <option value='July'>July</option>
          <option value='August'>August</option>
          <option value='September'>September</option>
          <option value='October'>October</option>
          <option value='November'>November</option>
          <option value='December'>December</option>
        </select>
        <input type='Submit' value='Generate Report' />
      </form>
    </div>
  </body>
</html>
