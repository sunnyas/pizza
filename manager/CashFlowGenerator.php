<?php
session_start();
if (!(isset($_SESSION["myusername"]) && isset($_SESSION["manager_login"]))) {
//    header("location:index.php");
}
?>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="frame.css" />
  </head>
  
  <body>
    <div id="header"></div>
    <div id="buttons">
      <div class="button">
        <a href="index.php">Home</a>
      </div>
    </div>
    <div class='choices'>
      <form name='cf_data' action='CashFlow.php' method='post'>

        <!-- time value for cash flow analysis -->
        Select a time window:<br> 
        <select name='cf_time' id='cf_time'>
          <option value='Hour'>Hour</option>
          <option value='Day'>Day</option>
          <option value='Week'>Week</option>
          <option value='Month'>Month</option>
          <option value='Year'>Year</option>
        </select>

        <input type='Submit' value='Generate Cash Flow Report' />
      </form>
    </div>
  </body>
</html>



