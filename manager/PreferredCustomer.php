<?php
session_start();
if (!(isset($_SESSION["myusername"]) && isset($_SESSION["manager_login"]))) {
//  header("location:index.php");
}
?>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="frame.css" />
  </head>
  <body>
    <div id="header"></div>
    <div id="buttons">
      <div class="button">
        <a href="index.php">Home</a>
      </div>
    </div>
    <div class='choices'>

      Preferred Customers:

      <?php
      //3. Preferred Customers Report:
      //top customers (in terms of revenue)
      //how often they order in the past 30 days 
      //money per category collected
      //average amount collected per delivery

      session_start();
      ob_start();
      mysql_connect("localhost", "semsc", "semsc") or die("cannot connect");
      mysql_select_db("pizza") or die("cannot select DB");

      $sql = "
      select count(orders.order_ID) as Number_Orders, 
        customer.username as Username, customer.Name_First as First_Name, customer.Name_Last as Last_Name,
        sum(orders.Price) as Total, sum(orders.Price_solids) as Drinks, sum(orders.Price_solids) as Food,
        sum(orders.Price) / count(orders.order_ID) as Average

      from orders, customer

      where customer.Username = orders.customer
        and DATE_SUB(CURRENT_DATE(), INTERVAL 120 DAY) <= orders.Paid_Time

      order by customer.Spent desc
      limit 0, 10";

      $result = mysql_query($sql);
            //printing relevant information
      echo "<table id=preferredUsers>
        <tr>
        <td>Number of Orders</td>
        <td>Username</td>
        <td>First Name</td>
        <td>Last Name</td>
        <td>Total</td>
        <td>Drinks</td>
        <td>Food</td>
        <td>Average per Order</td>
        </tr>";

      while ($row = mysql_fetch_array($result)) {
        echo "<tr>
          <td>" . $row['Number_Orders'] . "</td>
          <td>" . $row['Username'] . "</td>
          <td>" . $row['First_Name'] . "</td>
          <td>" . $row['Last_Name'] . "</td>
          <td>$" . $row['Total'] . "</td>
          <td>$" . $row['Drinks'] . "</td>
          <td>$" . $row['Food'] . "</td>
          <td>$" . $row['Average'] . "</td>
          </tr>";
      }
      echo "</table>";

      ob_end_flush();
      ?>
    </div>
  </body>
</html>



