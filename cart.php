<?php
session_start();
if (!(isset($_SESSION["myusername"]) && isset($_SESSION["customer_login"]))) {
    header("location:main_login.php");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cart</title>
        <link rel="stylesheet" type="text/css" href="frame.css" />
    </head>
    <center>
        <body>
            <div id ="header">

            </div>

            <div id="buttons">
                <div class="button">
                    <a href="index.php">Home</a>
                </div>
                <div class="button">
                    <a href="cart.php">Cart</a>
                </div>
                <div class="button">
                    <a href="order.php">Order</a>
                </div>
                <div class="button">
                    <?php
                    if (isset($_SESSION["myusername"])) {
                        echo "<a href='account.php'>Account</a>";
                    } else {
                        echo "<a href='register.php'>Register</a>";
                    }
                    ?>
                </div>
                <div class="button">
                    <?php
                    if (isset($_SESSION["myusername"])) {
                        echo "<a href='logout.php'>Logout</a>";
                    } else {
                        echo "<a href='main_login.php'>Login</a>";
                    }
                    ?>
                </div>
            </div>

            <form name="form1" method="post" action="cartupdate.php">
                <?php
                ob_start();
                $host = "localhost"; // Host name
                $username = "semsc"; // Mysql username
                $password = "semsc"; // Mysql password
                $db_name = "pizza"; // Database name
                mysql_connect($host, $username, $password) or die("cannot connect");
                mysql_select_db($db_name) or die("cannot select DB");

                $sql = "Select * from customer where Username'" . $_SESSION["myusername"] . "'";
                $result = mysql_query($sql);
                $row = mysql_fetch_array($result);
                $state = substr($row['Address'], -2);

                $total_solids = 0;
                $total_liquids = 0;
                $total_tax = 0;

                $sql = "Select orders.Order_ID, pizza.Pizza_ID, pizza.Name_Pizza, contains_solids.Quantity, pizza.Price
                from orders, pizza, contains_solids
                where orders.Received is null
                and orders.Order_ID=contains_solids.Order_ID
                and contains_solids.Pizza_ID=pizza.Pizza_ID
                and orders.Customer='" . $_SESSION["myusername"] . "'";
                $result = mysql_query($sql);
                $order_id = 0;
                echo "<table class='table1' width='820'>";
                echo "<tr><td>Delete</td><td>Name</td><td>Quantity</td><td>Price</td><td>Total</td></tr>";
                while ($row = mysql_fetch_array($result)) {
                    $order_id = $row['Order_ID'];
                    echo "<tr>";
                    echo "<td><input type='checkbox' name='pizzas[" . $row['Order_ID'] . "][" . $row['Pizza_ID'] . "]' value='true'></td>";
                    if (is_null($row['Name_Pizza'])) {
                        $sql = "select Topping_Name from contain where Pizza_ID='" . $row['Pizza_ID'] . "'";
                        $result2 = mysql_query($sql);
                        echo "<td>Custom Pizza:&nbsp;";
                        $count = mysql_num_rows($result2);
                        if ($count >= 1) {
                            $row2 = mysql_fetch_array($result2);
                            echo "<br>$row2[0]";
                            while ($row2 = mysql_fetch_array($result2)) {
                                echo ", $row2[0]";
                            }
                        } else {
                            echo "No Toppings";
                        }
                        echo "</td><td>" . $row['Quantity'] . "</td>";
                        echo "<td>" . $row['Price'] . "</td>";
                        $total = $row['Quantity'] * $row['Price'];
                        echo "<td>$total</td>";
                        $total_solids = $total_solids + $total;
                    } else {
                        echo "<td>" . $row['Name_Pizza'] . "</td>";
                        echo "<td>" . $row['Quantity'] . "</td>";
                        echo "<td>" . $row['Price'] . "</td>";
                        $sql = "Select Time_start from happy_hour where Pizza='" . $row['Pizza_ID'] . "'";
                        $result2 = mysql_query($sql);
                        $total = $row['Quantity'] * $row['Price'];
                        $hh = false;
                        while ($row2 = mysql_fetch_array($result2)) {
                            $time1 = strtotime($row2[0]);
                            $diff = strtotime("now") - $time1;
                            if ($diff >= 0 && $diff < 3600) {
                                $total = ceil($row['Quantity'] / 2.0) * $row['Price'];
                                $hh = true;
                            }
                        }
                        if ($hh) {
                            echo "<td>$total (HH)</td>";
                        } else {
                            echo "<td>$total</td>";
                        }
                        $total_solids = $total_solids + $total;
                    }
                    echo "</tr>";
                }

                $sql = "Select orders.Order_ID, contains_fluids.Drink_Name, contains_fluids.Quantity, drink.Cost
                from orders, contains_fluids, drink
                where orders.Received is null
                and orders.Order_ID=contains_fluids.Order_ID
                and contains_fluids.Drink_Name=drink.Drink_Name
                and orders.Customer='" . $_SESSION["myusername"] . "'";
                $result = mysql_query($sql);
                while ($row = mysql_fetch_array($result)) {
                    $order_id = $row['Order_ID'];
                    echo "<tr>";
                    echo "<td><input type='checkbox' name='drinks[" . $row['Order_ID'] . "][" . $row['Drink_Name'] . "]' value='true'></td>";
                    echo "<td>" . $row['Drink_Name'] . "</td>";
                    echo "<td>" . $row['Quantity'] . "</td>";
                    echo "<td>" . $row['Cost'] . "</td>";
                    $total = $row['Cost'] * $row['Quantity'];
                    echo "<td>$total</td>";
                    $total_liquids = $total_liquids + $total;
                    if ($state == 'MD') {
                        $total_tax = round($total_liquids * .06, 2);
                    } else {
                        $total_tax = round($total_liquids * .025, 2);
                    }
                }
                if ($order_id != 0) {
                    echo "<tr>";
                    echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                    echo "<td>Pizzas</td><td>$total_solids</td>";
                    echo "</tr>";

                    echo "<tr>";
                    echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                    echo "<td>Drinks</td><td>$total_liquids</td>";
                    echo "</tr>";

                    echo "<tr>";
                    echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                    echo "<td>Taxes</td><td>$total_tax</td>";
                    echo "</tr>";

                    $total = $total_liquids + $total_solids + $total_tax;

                    echo "<tr>";
                    echo "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                    echo "<td>Total</td><td>$total</td>";
                    echo "</tr>";

                    echo "<tr>";
                    echo "<td colspan='5'>";
                    echo "<input type='radio' name='payment' value='cash' checked='yes'>Cash&nbsp;&nbsp;&nbsp;";
                    echo "<input type='radio' name='payment' value='check'>Check&nbsp;&nbsp;&nbsp;";
                    echo "<input type='radio' name='payment' value='credit_card'>Credit Card&nbsp;&nbsp;&nbsp;";
                    echo "</td>";
                    echo "</tr>";

                    echo "<tr>";
                    echo "<td colspan='1'><input type='submit' name='order' value='Delete'></td>";
                    echo "<td>&nbsp;</td><td>&nbsp;</td>";
                    echo "<td colspan='2'><input type='submit' name='order' value='Order' label='ORDER!'></td>";
                    echo "</tr>";
                }

                echo "</table>";

                $_SESSION['order_id'] = $order_id;
                $_SESSION['total_liq'] = $total_liquids;
                $_SESSION['total_sol'] = $total_solids;
                $_SESSION['total_tax'] = $total_tax;
                $_SESSION['state'] = $state;

                ob_end_flush();
                ?>
            </form>
        </body>
    </center>
</html>
