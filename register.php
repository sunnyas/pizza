<?php
session_start();
if ((isset($_SESSION["myusername"]) && isset($_SESSION["customer_login"]))) {
    header("location:index.php");
}
if (isset($_SESSION["manager_login"])) {
    unset($_SESSION["manager_login"]);
}
if (isset($_SESSION["employee_login"])) {
    unset($_SESSION["employee_login"]);
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register</title>
        <link rel="stylesheet" type="text/css" href="frame.css" />
        <script src="form_functions.js" type="text/javascript"></script>
        <script type="text/javascript">
            function onRegister(){
                var user = document.getElementById("myusername");
                var pass = document.getElementById("mypassword");
                var first = document.getElementById("firstname");
                var last = document.getElementById("lastname");
                var credit = document.getElementById("creditcard");
                var address = document.getElementById("address");
                if(lengthRestriction(user, 2, 20) 
                    && lengthRestriction(pass, 2, 20) 
                    && lengthRestriction(first, 2, 20) 
                    && lengthRestriction(last, 2, 20) 
                    && lengthRestriction(credit, 12, 19) 
                    && lengthRestriction(address, 2, 35) 
                    && loginSensitive(user,"Invalid Username") 
                    && loginSensitive(pass,"Invalid Password")
                    && loginSensitive(first,"Invalid First")
                    && loginSensitive(last,"Invalid Last")
                    && isNumeric(credit,"Invalid Credit Card #")){
                    document.forms["form1"].submit();
                }
            }
        </script>
    </head>
    <center>
        <body>
            <div id ="header">

            </div>

            <div id="buttons">
                <div class="button">
                    <a href="index.php">Home</a>
                </div>
                <div class="button">
                    <a href="cart.php">Cart</a>
                </div>
                <div class="button">
                    <a href="order.php">Order</a>
                </div>
                <div class="button">
                    <?php
                    if (isset($_SESSION["myusername"])) {
                        echo "<a href='account.php'>Account</a>";
                    } else {
                        echo "<a href='register.php'>Register</a>";
                    }
                    ?>
                </div>
                <div class="button">
                    <?php
                    if (isset($_SESSION["myusername"])) {
                        echo "<a href='logout.php'>Logout</a>";
                    } else {
                        echo "<a href='main_login.php'>Login</a>";
                    }
                    ?>
                </div>
            </div>

            <div id="login">
                <table width="300" border="0" align="center" cellpadding="0" cellspacing="1">
                    <tr>
                    <form name="form1" method="post" action="register_check.php">
                        <td>
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td colspan="3"><strong>Member Register</strong></td>
                                </tr>
                                <tr>
                                    <td width="78">Username</td>
                                    <td width="6">:</td>
                                    <td width="294"><input name="myusername" type="text" id="myusername"></td>
                                </tr>
                                <tr>
                                    <td>Password</td>
                                    <td>:</td>
                                    <td><input name="mypassword" type="password" id="mypassword"></td>
                                </tr>
                                <tr>
                                    <td>First Name</td>
                                    <td>:</td>
                                    <td><input name="firstname" type="text" id="firstname"></td>
                                </tr>
                                <tr>
                                    <td>Last Name</td>
                                    <td>:</td>
                                    <td><input name="lastname" type="text" id="lastname"></td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td>:</td>
                                    <td><input name="address" type="text" id="address"></td>
                                </tr>
                                <tr>
                                    <td>State</td>
                                    <td>:</td>
                                    <td><select name="state" id="state">
                                            <option value='MD'>MD</option>
                                            <option value='VA'>VA</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td>Credit Card</td>
                                    <td>:</td>
                                    <td><input name="creditcard" type="text" id="creditcard"></td>
                                </tr>
                                <tr>
                                    <td colspan='3'><button type='button' onclick="javascript:onRegister();">Register</button></td>
                                </tr>
                                <?php
                                if (isset($_SESSION['register_fail'])) {

                                    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td><font color=#FF0000>Failed user already exists</td></tr>";

                                    unset($_SESSION['register_fail']);
                                }
                                ?>
                            </table>
                        </td>
                    </form>
                    </tr>
                </table>
            </div>
        </body>
    </center>
</html>
