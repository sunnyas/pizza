function notEmpty(elem, helperMsg){
    if(elem.value.length == 0){
        alert(helperMsg);
        elem.focus(); // set the focus to this input
        return false;
    }
    return true;
}
function isNumeric(elem, helperMsg){
    var numericExpression = /^[0-9]+$/;
    if(elem.value.match(numericExpression)){
        return true;
    }else{
        alert(helperMsg);
        elem.focus();
        return false;
    }
}
function lengthRestriction(elem, min, max){
    var uInput = elem.value;
    if(uInput.length >= min && uInput.length <= max){
        return true;
    }else{
        alert("Please enter between " +min+ " and " +max+ " characters");
        elem.focus();
        return false;
    }
}

function loginSensitive(elem, helperMsg) {
    var numericExpression = /^[0-9a-zA-Z]+$/;
    if(elem.value.match(numericExpression)){
        return true;
    }else{
        alert(helperMsg);
        elem.focus();
        return false;
    }
}

function submitLogin(){
    var user = document.getElementById("myusername");
    var pass = document.getElementById("mypassword");
    if(lengthRestriction(user, 2, 20) 
        && lengthRestriction(pass, 2, 20) 
        && loginSensitive(user,"Invalid Username") 
        && loginSensitive(pass,"Invalid Password")){
        document.forms["form1"].submit();
    }
}